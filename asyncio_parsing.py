#!/usr/bin/python3
# -*- coding: utf-8 -*-
import asyncio
import enum
import aiohttp
import netaddr

class Status(enum.Enum):
    unknown = "Never used account"
    normal = "Usual account"
    paid = "Paid account"

async def test(session, mac):
    """
    :param aiohttp.ClientSession session: HTTP client session
    :param str mac: MAC address
    """
    eui = netaddr.EUI(mac)
    URL = "https://login.wi-fi.ru/am/UI/Login?org=mac&service=coa&client_mac={}&ForceAuth=true".format(str(eui).lower())
    async with session.get(URL) as response:
        text = await response.text()
        if "Введите Ваш номер и подтвердите идентификацию" in text:
            status = Status.unknown
        elif "mosmetro_premium" in text:
            status = Status.paid
        else:
            status = Status.normal
        print("{}\t{}".format(mac, status.value))

async def bound_test(session, sem, mac):
    """
    :param aiohttp.ClientSession session: HTTP client session
    :param asyncio.Semaphore sem: Semaphore
    :param str mac: MAC address
    """
    async with sem:
        await test(session, mac)

async def run(loop, macs):
    """
    :param asyncio.BaseEventLoop loop: Event loop
    :param list macs: MAC addresses to test
    """
    tasks = []
    sem = asyncio.Semaphore(32)
    async with aiohttp.ClientSession(loop=loop) as session:
        for mac in macs:
            mac = netaddr.EUI(mac)
            task = asyncio.ensure_future(bound_test(session, sem, mac))
            tasks.append(task)
        results = asyncio.gather(*tasks)
        await results

if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    with open("parsed_macs.txt", "r") as fd:
        macs = [l.strip() for l in fd.readlines()]
    loop.run_until_complete(run(loop, macs))
